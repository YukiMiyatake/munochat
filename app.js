
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

var v =   require("hello/build/Release/hello");
console.log( v.print() );


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

//var Hello = new require('hello');
//  , hello = new Hello();
//hello();
//var hello =  require("./build/Release/hello");
//hello.hello();
/*
var MeCab = new require('mecab-async')
  , mecab = new MeCab()
;
mecab.parse('すもももももももものうち', function(err, result) {
	if (err) throw err;
	console.log(result);
});
*/